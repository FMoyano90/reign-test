import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';

import { DateAgoPipe } from './pipes/date-ago.pipe';
import { RouterModule, Routes } from '@angular/router';

@NgModule({
  declarations: [AppComponent, DateAgoPipe],
  imports: [
    BrowserModule,
    MatTableModule,
    MatIconModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot([]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
