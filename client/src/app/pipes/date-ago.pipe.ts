import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateAgo',
  pure: true,
})
export class DateAgoPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value) {
      let meses = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec',
      ];
      let fechaValue = new Date(value);
      let fechaValueMes = meses[fechaValue.getMonth()];
      let fechaValueDia = fechaValue.getDate();
      let fechaValueHora = fechaValue.getHours();
      let fechaValueMinuto = fechaValue.getMinutes();
      let fechaActual = new Date();
      let fechaActualMes = meses[fechaActual.getMonth()];
      let fechaActualDia = fechaActual.getDate();

      if (
        fechaValueDia === fechaActualDia &&
        fechaValueMes === fechaActualMes
      ) {
        if (fechaValueHora >= 0 && fechaValueHora <= 12) {
          if (fechaValueMinuto >= 0 && fechaValueMinuto <= 9) {
            return `${fechaValueHora}:0${fechaValueMinuto} am`;
          } else {
            return `${fechaValueHora}:${fechaValueMinuto} am`;
          }
        } else {
          return `${fechaValueHora}:${fechaValueMinuto} pm`;
        }
      } else if (fechaActualDia - 1 == fechaValueDia) {
        return 'Yesterday';
      } else {
        return `${fechaValueMes} ${fechaValueDia}`;
      }
    }
  }
}
