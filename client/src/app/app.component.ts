import { Component, OnInit } from '@angular/core';
import { Noticia } from './models/noticia.model';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public noticias: Noticia[];
  public displayedColumns: string[] = ['story_title', 'created_at', 'delete'];
  public dataSource;

  ngOnInit(): void {
    this.obtenerNoticias();
  }

  async obtenerNoticias() {
    await ajax.getJSON('http://localhost:3010/news').subscribe(
      (resp) => {
        this.noticias = resp['news'];
        this.dataSource = this.noticias;
      },
      (err) => console.log(err)
    );
  }

  abrirNoticia(story_url: string, url: string) {
    if (story_url && story_url != '') {
      window.open(story_url, '_blank');
    } else if (!story_url && url && url != '') {
      window.open(url, '_blank');
    } else {
      console.log('No existe URL');
    }
  }

  async borrarNoticia(id) {
    await ajax.delete('http://localhost:3010/news/' + id).subscribe(
      (resp) => {
        console.log(resp);
        this.obtenerNoticias();
      },
      (err) => console.log(err)
    );
  }
}
